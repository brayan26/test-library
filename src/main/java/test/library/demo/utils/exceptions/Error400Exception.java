package test.library.demo.utils.exceptions;

public class Error400Exception extends RuntimeException {
    public Error400Exception(String message) {
        super(message);
    }
}
