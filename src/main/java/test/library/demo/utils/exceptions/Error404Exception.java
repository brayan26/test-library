package test.library.demo.utils.exceptions;

public class Error404Exception extends RuntimeException {
    public Error404Exception(String message) {
        super(message);
    }
}
