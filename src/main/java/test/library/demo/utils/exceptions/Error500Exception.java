package test.library.demo.utils.exceptions;

public class Error500Exception extends RuntimeException {
    public Error500Exception(String message) {
        super(message);
    }
}
