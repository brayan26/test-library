package test.library.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBrayanApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBrayanApplication.class, args);
	}

}
